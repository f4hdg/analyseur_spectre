#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 10 15:47:06 2021

@author: nop
"""

import tkinter as tk
from tkinter import ttk

from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)

import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor
import numpy as np

import serial, time, struct
from threading import Thread


class PyLTDZ(tk.Tk): #Frame):
    
    def __init__(self, master=None):
        #super().__init__(master)
        tk.Tk.__init__(self,master)
        self.master = master
        #self.grid()
        
        
        self.Var_init()
        self.create_widgets()
        self.Graph_init()
        self.Graph_VFO_init()
        #self.Cursor_init()
        
        self.resizable(False,False)
		
	################################### VARIABLE
    def Var_init(self):
        ### Variable VFO
        self.freq_VFO = tk.DoubleVar()
        
        self.freq_VFO.set(100)
        self.VFO_continue = False
        
        ### Variable Spectre
        self.freq_debut = tk.DoubleVar()
        self.freq_fin = tk.DoubleVar()
        self.nbre_point = tk.IntVar()
        self.port= tk.StringVar()

        self.freq_debut.set(88.0)
        self.freq_fin.set(110.0)
        self.nbre_point.set(200)
        self.port.set("/dev/ttyUSB0")
        self.Mesure_continue_index = 0
        
        self.Curseur_tab = [[ 1, 0, 0, "v", 0 ],
							[ 1, 0, 0, "o", 0 ],
							[ 1, 0, 0, "p", 0 ],
							[ 1, 0, 0, "x", 0 ],
							]
        self.Curseur_select = tk.IntVar();  ## Quel curseur est utilisé
        self.Curseur_select.set(0)
        self.Curseur1_valid = tk.IntVar();	## Curseur on/off
        self.Curseur1_valid.set(0)
        self.Curseur2_valid = tk.IntVar();	## Curseur on/off
        self.Curseur2_valid.set(0)
        self.Balayage_continue = False
        
        self.Spectre = [(-80.0),]
        for i in range(1, self.nbre_point.get()) :
            self.Spectre.append(-80.0)
        
        self.Puissance = [(-80.0),]
        for i in range(1, self.nbre_point.get()) :
            self.Puissance.append(-80.0)
            
	
	################################### GUI
    def create_widgets(self):

        style = ttk.Style(self)
        style.configure("lefttab.TNotebook", tabposition="n")
        
        self.notebook = ttk.Notebook(self, style="lefttab.TNotebook")
 
        self.spectre_tab = tk.Frame(self.notebook)
        self.VFO_tab =     tk.Frame(self.notebook)
 
        self.notebook.add(self.spectre_tab, text="Analyseur de Spectre")
        self.notebook.add(self.VFO_tab, text="Générateur")
        self.notebook.grid(row=0, column=0, sticky="nw")
        self.create_spectre_GUI()
        self.create_VFO_GUI()
    
    
    ################################### VFO GUI
    def  create_VFO_GUI(self):
    	self.VFO_Freq_Frame = tk.LabelFrame(self.VFO_tab, text = "Frequence", padx=20, pady=20, font = "Helvetica 12 bold")
    	self.VFO_Freq_Frame.grid(row=0, column=0)
    	
    	self.VFO_Mesure_Frame = tk.LabelFrame(self.VFO_tab, text = "Puissance", padx=20, pady=20, font = "Helvetica 12 bold")
    	self.VFO_Mesure_Frame.grid(row=1, column=0)
    	
    	self.VFO_port_Frame = tk.LabelFrame(self.VFO_tab, text = "Port", padx=20, pady=20, font = "Helvetica 12 bold")
    	self.VFO_port_Frame.grid(row=2, column=0)
    	
    	self.VFO_graph_Frame = tk.Frame(self.VFO_tab, padx=20, pady=20)
    	self.VFO_graph_Frame.grid(row=0, column=2, rowspan = 3)
    	
    	self.VFO_control_Frame = tk.LabelFrame(self.VFO_tab, padx=20, pady=20, font = "Helvetica 12 bold")
    	self.VFO_control_Frame.grid(row=2, column=3)
    	
    	    	
    	
    	self.entry_VFO_Freq = tk.Entry(self.VFO_Freq_Frame, textvariable=self.freq_VFO)
    	self.entry_VFO_Freq.grid(row=0, column=0)
    	
    	self.label_Mesure_VFO = tk.Label(self.VFO_Mesure_Frame, text = "-80.0 dBm", anchor="center",fg="white",bg="blue", font = "Helvetica 30 bold", width=11)#, textvariable=self.mesure_VFO)
    	self.label_Mesure_VFO.grid(row=0, column=0)
    	
    	## PORT
    	self.label_VFO_port = tk.Label(self.VFO_port_Frame, text = "Port serie")
    	self.label_VFO_port.grid(row=0, column=0)        
    	self.entry_VFO_port = tk.Entry(self.VFO_port_Frame, textvariable=self.port)
    	self.entry_VFO_port.grid(row=0, column=1)
    	self.version_VFO = tk.StringVar()
    	self.button_VFO_port_test = tk.Button(self.VFO_port_Frame, text="Test Serial", command=self.LTDZ_version)
    	self.button_VFO_port_test.grid(row=1, column=0)
    	self.label_VFO_ltdz_version = tk.Label(self.VFO_port_Frame, textvariable=self.version) 
    	self.label_VFO_ltdz_version.grid(row=1, column=1)
        
    	## GRAPHIC 
    	plt.style.use('dark_background')
    	plt.rcParams['axes.grid'] = True
    	self.canvas_fig_VFO, self.canvas_ax_VFO = plt.subplots()  # Create a figure containing a single axes.
    	#self.canvas_fig_VFO.canvas.mpl_connect('button_press_event', self.Curseur_Click)
    	
    	self.canvas_VFO = FigureCanvasTkAgg(self.canvas_fig_VFO, self.VFO_graph_Frame)  # A tk.DrawingArea.
    	self.canvas_VFO.draw()
    	self.canvas_VFO.get_tk_widget().grid(row = 0, column = 0) #pack(fill=tk.BOTH, expand=tk.YES)
    	#self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
    	self.toolbar_VFO = NavigationToolbar2Tk(self.canvas_VFO, self.VFO_graph_Frame, pack_toolbar=False)
    	self.toolbar_VFO.update()
    	self.toolbar_VFO.grid(row = 1, column = 0) #pack()
    	#self.cursor =  Cursor(self.canvas_ax, horizOn = True, vertOn=True, color='red', linewidth=1, useblit=True)
       
    	
    	### CONTROLE
    	self.Button_VFO_continue = tk.Button(self.VFO_control_Frame, text="Continue", command=self.LTDZ_VFO_continue)
    	self.Button_VFO_continue.grid(row=0, column=0) #grid(row=0,column=control_col) #.pack(side=tk.BOTTOM)
    	self.Button_VFO_simple = tk.Button(self.VFO_control_Frame, text="Simple", command=self.LTDZ_VFO_simple)
    	self.Button_VFO_simple.grid(row=0, column=1) #grid(row=0,column=control_col) #.pack(side=tk.BOTTOM)
    	
    	### Quitter
    	self.Button_VFO_quitter = tk.Button(self.VFO_control_Frame, text="Quit", command=self.destroy)
    	self.Button_VFO_quitter.grid(row=1, column=2)#grid(row=1, column=control_col) #pack(side=tk.BOTTOM)    
    
    ################################### SPECTRUM GUI 
    def create_spectre_GUI(self) :    
        self.Freq_Frame = tk.LabelFrame(self.spectre_tab, text = "Frequence", padx=20, pady=20, font = "Helvetica 12 bold")
        self.Freq_Frame.grid(row=0, column=0)

        self.Port_Frame = tk.LabelFrame(self.spectre_tab, text = "Port", padx=20, pady=20, font = "Helvetica 12 bold")
        self.Port_Frame.grid(row=1, column=0)

        self.Graphic_Frame = tk.Frame(self.spectre_tab, padx=20, pady=20)
        self.Graphic_Frame.grid(row=0, column=2, rowspan = 2)
        
        self.Marker_Frame = tk.LabelFrame(self.spectre_tab, text="Marker", padx=20, pady=20, font = "Helvetica 12 bold")
        self.Marker_Frame.grid(row=0,column=3)
        
        self.Control_Frame = tk.LabelFrame(self.spectre_tab, padx=20, pady=20,)
        self.Control_Frame.grid(row=1, column=3)

        ## FRAME FREQUENECE
        self.label_freq_debut = tk.Label(self.Freq_Frame, text = "Frequence Debut")
        self.label_freq_debut.grid(row=0, column=0)
        self.entry_freq_debut = tk.Entry(self.Freq_Frame, textvariable=self.freq_debut)
        self.entry_freq_debut.grid(row=0, column=1)
        
        self.label_freq_fin = tk.Label(self.Freq_Frame, text = "Frequence Fin")
        self.label_freq_fin.grid(row=1, column=0)
        self.entry_freq_fin = tk.Entry(self.Freq_Frame, textvariable=self.freq_fin)
        self.entry_freq_fin.grid(row=1, column=1)
        
        self.label_nbre_point = tk.Label(self.Freq_Frame, text = "Nombre Point")
        self.label_nbre_point.grid(row=2, column=0)
        self.entry_nbre_point = tk.Entry(self.Freq_Frame, textvariable=self.nbre_point)
        self.entry_nbre_point.grid(row=2, column=1)
        
        self.label_port = tk.Label(self.Port_Frame, text = "Port serie")
        self.label_port.grid(row=0, column=0)        
        self.entry_port = tk.Entry(self.Port_Frame, textvariable=self.port)
        self.entry_port.grid(row=0, column=1)
        self.version = tk.StringVar()
        
        self.button_port_test = tk.Button(self.Port_Frame, text="Test Serial", command=self.LTDZ_version)
        self.button_port_test.grid(row=1, column=0)
        self.label_ltdz_version = tk.Label(self.Port_Frame, textvariable=self.version) 
        self.label_ltdz_version.grid(row=1, column=1)
        
        ### Frame GRAPHIC 
        plt.style.use('dark_background')
        plt.rcParams['axes.grid'] = True
        self.canvas_fig, self.canvas_ax = plt.subplots()  # Create a figure containing a single axes.
        self.canvas_fig.canvas.mpl_connect('button_press_event', self.Curseur_Click)
        
        
        self.canvas_ax.grid(which='both')   
        self.canvas = FigureCanvasTkAgg(self.canvas_fig, self.Graphic_Frame)  # A tk.DrawingArea.
        #self.canvas.get_tk_widget().config(width=700,height=600)
        
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(fill=tk.BOTH, expand=tk.YES)
        #self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
        self.toolbar = NavigationToolbar2Tk(self.canvas, self.Graphic_Frame, pack_toolbar=False)
        self.toolbar.update()
        self.toolbar.pack()
        self.cursor =  Cursor(self.canvas_ax, horizOn = True, vertOn=True, color='red', linewidth=1, useblit=True)
        
        ### FRAME MARKER       
        self.Curseur1_label = tk.Label(self.Marker_Frame, text="1")
        self.Curseur1_label.grid(row=0, column=0)
        self.Curseur1_select = tk.Radiobutton(self.Marker_Frame, variable = self.Curseur_select, value = 0, command=self.Curseur_select_change)
        self.Curseur1_select.grid(row=0, column=1)
        self.Curseur1_toggle = tk.Checkbutton(self.Marker_Frame,  variable=self.Curseur1_valid, command=self.Curseur_toggle)
        self.Curseur1_toggle.grid(row=0, column=2)
        self.Curseur1_labelX = tk.Label(self.Marker_Frame, text="0.0")
        self.Curseur1_labelX.grid(row=0, column=3)
        self.Curseur1_labelY = tk.Label(self.Marker_Frame, text="1.0")
        self.Curseur1_labelY.grid(row=0, column=4)
        
        
        self.Curseur2_label = tk.Label(self.Marker_Frame, text="2")
        self.Curseur2_label.grid(row=1, column=0)
        self.Curseur2_select = tk.Radiobutton(self.Marker_Frame, variable = self.Curseur_select, value = 1, command=self.Curseur_select_change)
        self.Curseur2_select.grid(row=1, column=1)
        self.Curseur2_toggle = tk.Checkbutton(self.Marker_Frame, variable = self.Curseur2_valid, command=self.Curseur_toggle)
        self.Curseur2_toggle.grid(row=1, column=2)
        self.Curseur2_labelX = tk.Label(self.Marker_Frame, text="0.0")
        self.Curseur2_labelX.grid(row=1, column=3)
        self.Curseur2_labelY = tk.Label(self.Marker_Frame, text="1.0")
        self.Curseur2_labelY.grid(row=1, column=4)
        
        
        ### FRAME CONTROLE
        self.Button_balayage_continue = tk.Button(self.Control_Frame, text="Continue", command=self.LTDZ_balayage_continue)
        self.Button_balayage_continue.grid(row=0, column=0) #grid(row=0,column=control_col) #.pack(side=tk.BOTTOM)
        self.Button_balayage_simple = tk.Button(self.Control_Frame, text="Simple", command=self.LTDZ_balayage_simple)
        self.Button_balayage_simple.grid(row=0, column=1) #grid(row=0,column=control_col) #.pack(side=tk.BOTTOM)
        # ~ self.Button_balayage_stop = tk.Button(self.Control_Frame, bitmap="error", text="STOP", command=self.LTDZ_balayage_stop)
        # ~ self.Button_balayage_stop.grid(row=0, column=2) #grid(row=0,column=control_col) #.pack(side=tk.BOTTOM)
        self.Button_quitter = tk.Button(self.Control_Frame, text="Quit", command=self.destroy)
        self.Button_quitter.grid(row=1, column=3)#grid(row=1, column=control_col) #pack(side=tk.BOTTOM)
        
        
    
   ################################### VFO     
    def LTDZ_VFO_simple(self):
        BAUDRATE = 57600
        try :
            self.ser = serial.Serial(self.port.get(), BAUDRATE)
        except Exception:
            tk.messagebox.showerror("Erreur Serial", "le port serie n'existe pas")
            print ("error open serial port: " + str(self.port.get()))
            #self.destroy()
        else :
            freq = self.freq_VFO.get() * 1e6
            commande = "f" + format(int(freq/10.0),'09')            
            self.ser.write(b'\x8f' + (commande).encode())
            # ~ time.sleep(0.5)
            self.ser.write(b'\x8f' + "m".encode() )
            data = self.ser.read(4)
            val_tab = struct.unpack("<2h", data)
            power= (((0.19) * val_tab[0]) - 87.0)               
            self.label_Mesure_VFO.configure(text= "{:.2f} dBm".format(power))
            self.Puissance.append(power)
            self.Puissance.pop(0)
            self.Graph_VFO_dessin()
        
               
	
    def LTDZ_VFO_continue(self):
        if self.VFO_continue : 
            self.VFO_continue = False
            self.Button_VFO_continue.configure( relief = tk.GROOVE)
        else :
            self.VFO_continue = True
            self.Button_VFO_continue.configure( relief = tk.SUNKEN )
            mesure_VFO = Thread (target = self.LTDZ_VFO_contiue_mesure)
            mesure_VFO.start()  

    def LTDZ_VFO_contiue_mesure(self):
        #if Balayage_continue == True : 
        BAUDRATE = 57600
        try :
            self.ser = serial.Serial(self.port.get(), BAUDRATE)
        except Exception:
            tk.messagebox.showerror("Erreur Serial", "le port serie n'existe pas")
            print ("error open serial port: " + str(self.port.get()))
            #self.destroy()
        else :
            self.ser.reset_input_buffer()
            time.sleep(1)
            while True :
                freq = self.freq_VFO.get() * 1e6
                commande = "f" + format(int(freq/10.0),'09')            
                self.ser.write(b'\x8f' + (commande).encode())
                # ~ time.sleep(0.5)
                self.ser.write(b'\x8f' + "m".encode() )
                data = self.ser.read(4)
                val_tab = struct.unpack("<2h", data)
                power= (((0.19) * val_tab[0]) - 87.0)               
                self.label_Mesure_VFO.configure(text= "{:.2f} dBm".format(power))
                self.Puissance.append(power)
                self.Puissance.pop(0)
                self.Graph_VFO_dessin()
                
                if self.VFO_continue == False :
                    break
            self.ser.close()
	    
	    	
    def Graph_VFO_init(self):
        self.canvas_ax_VFO.clear()
        self.canvas_ax_VFO.plot(self.Puissance)
        self.canvas_VFO.draw()
        
    def Graph_VFO_dessin(self):
        self.canvas_ax_VFO.clear()
        self.canvas_ax_VFO.plot(self.Puissance)
        # ~ if self.Curseur_tab[0][4] and self.Curseur_tab[0][1] > 0 : 
            # ~ self.canvas_ax.plot(self.Curseur_tab[0][1], self.Curseur_tab[0][2], marker=self.Curseur_tab[0][3])
            # ~ self.Curseur1_labelX.configure(text="{:.2e} MHz".format(self.Curseur_tab[0][1]/1e6))
            # ~ self.Curseur1_labelY.configure(text=str(self.Curseur_tab[0][2]))
        # ~ if self.Curseur_tab[1][4] and self.Curseur_tab[1][1] > 0  : 
            # ~ self.canvas_ax.plot(self.Curseur_tab[1][1], self.Curseur_tab[1][2], marker=self.Curseur_tab[1][3])
            # ~ self.Curseur2_labelX.configure(text="{:.2e} MHz".format(self.Curseur_tab[0][1]/1e6))
            # ~ self.Curseur2_labelY.configure(text=str(self.Curseur_tab[1][2]))
        self.canvas_VFO.draw()        
	
	################################### SPECTRUM
	
    # def Cursor_init(self):
    #     cursor = [[ 1, 0, 0, "v", False ],] # numero marker , X , Y , Type Marker
    #     self.Curseur_tab = cursor
    #     # self.ax.plot(event.xdata, self.Spectre[index], marker="v")
    #     # self.canvas.draw()
    #     self.Graph_dessin()
        
    def Curseur_toggle(self):
        self.Curseur_tab[0][4] = self.Curseur1_valid.get()
        self.Curseur_tab[1][4] = self.Curseur2_valid.get()
	    
    def Curseur_select_change(self):
        print("curseur : " + str(self.Curseur_select.get())) 
     
    def Curseur_Click(self, event):
        index = int(((event.xdata /1e6)- self.freq_debut.get())/((self.freq_fin.get() - self.freq_debut.get())/len(self.Spectre)))
        if index >= 0 and index < len(self.Spectre) :
            self.Curseur_tab[self.Curseur_select.get()][1] = event.xdata
            self.Curseur_tab[self.Curseur_select.get()][2] = self.Spectre[index]
        self.Graph_dessin()

    
    def Graph_init(self):
        freqdebut = self.freq_debut.get() * 1e6
        freqfin = self.freq_fin.get() * 1e6
        self.canvas_ax.clear()
        freq = np.arange(freqdebut, freqfin, (freqfin - freqdebut)/self.nbre_point.get())
        self.canvas_ax.plot(freq, self.Spectre)
        self.canvas.draw()
        
    def Graph_dessin(self):
        freqdebut = self.freq_debut.get() * 1e6
        freqfin = self.freq_fin.get() * 1e6
        self.canvas_ax.clear()
        freq = np.arange(freqdebut, freqfin, (freqfin - freqdebut)/self.nbre_point.get())
        self.canvas_ax.plot(freq, self.Spectre)
        if self.Curseur_tab[0][4] and self.Curseur_tab[0][1] > 0 : 
            self.canvas_ax.plot(self.Curseur_tab[0][1], self.Curseur_tab[0][2], marker=self.Curseur_tab[0][3])
            self.Curseur1_labelX.configure(text="{:.2e} MHz".format(self.Curseur_tab[0][1]/1e6))
            self.Curseur1_labelY.configure(text=str(self.Curseur_tab[0][2]))
        if self.Curseur_tab[1][4] and self.Curseur_tab[1][1] > 0  : 
            self.canvas_ax.plot(self.Curseur_tab[1][1], self.Curseur_tab[1][2], marker=self.Curseur_tab[1][3])
            self.Curseur2_labelX.configure(text="{:.2e} MHz".format(self.Curseur_tab[0][1]/1e6))
            self.Curseur2_labelY.configure(text=str(self.Curseur_tab[1][2]))
        self.canvas.draw()        
    
    def LTDZ_contiue_mesure(self) :#self,ser,freq):
        #if Balayage_continue == True : 
        BAUDRATE = 57600
        try :
            self.ser = serial.Serial(self.port.get(), BAUDRATE)
        except Exception:
            tk.messagebox.showerror("Erreur Serial", "le port serie n'existe pas")
            print ("error open serial port: " + str(self.port.get()))
            #self.destroy()
        else :
            self.ser.reset_input_buffer()
            time.sleep(1)
            while True :
                freq = 1e6 * (self.freq_debut.get() + self.Mesure_continue_index *((self.freq_fin.get() - self.freq_debut.get()) / self.nbre_point.get()))
                commande = "f" + format(int(freq/10.0),'09')
                self.ser.write(b'\x8f' + (commande).encode())
                # ~ time.sleep(0.5)
                self.ser.write(b'\x8f' + "m".encode() )
                data = self.ser.read(4)
                val_tab = struct.unpack("<2h", data)
                power= (((0.19) * val_tab[0]) - 87.0)               
                self.Mesure_continue_index += 1
                if self.Mesure_continue_index >= self.nbre_point.get() :
                    self.Mesure_continue_index = 0
                self.Spectre[self.Mesure_continue_index] = power
               
                self.Graph_dessin()
               	# ~ print (freq)
               
                if self.Balayage_continue == False :
                    break
            self.ser.close()

        
    def LTDZ_balayage_stop(self):
        self.Balayage_continue = False

    def LTDZ_balayage_continue(self):
        if self.Balayage_continue : 
            self.Balayage_continue = False
            self.Button_balayage_continue.configure( relief = tk.GROOVE)
        else :
            self.Balayage_continue = True
            self.Mesure_continue_index = 0
            self.Button_balayage_continue.configure( relief = tk.SUNKEN )
            mesure = Thread (target = self.LTDZ_contiue_mesure)
            mesure.start()        

    def LTDZ_balayage_simple(self):
        BAUDRATE = 57600
        try :
            self.ser = serial.Serial(self.port.get(), BAUDRATE)
        except Exception:
            tk.messagebox.showerror("Erreur Serial", "le port serie n'existe pas")
            print ("error open serial port: " + str(self.port.get()))
            #self.destroy()
        else :
            self.ser.reset_input_buffer()
            time.sleep(1)
            freqdebut = self.freq_debut.get() * 1e6
            freqfin = self.freq_fin.get() * 1e6
        
            pas = (freqfin - freqdebut)/self.nbre_point.get()
            commande = "a" + format(int(freqdebut/10.0),'09') +  format(int(pas/10.0),'08') + format(int(self.nbre_point.get()),'04') + format(int(50),'03')
            self.ser.write(b'\x8f')
            self.ser.write((commande).encode())

            data = bytes('', encoding='utf8')
            while (self.ser.inWaiting() > 0) or len(data) < (self.nbre_point.get() * 4) :    
                data += self.ser.read(self.ser.inWaiting())
                time.sleep(0.5)

            longeur = int(len(data)/2)
            val_tab = struct.unpack('<' + str(longeur) + 'h', data)
            val_tab_ch0 = [val_tab[0],]
            power_tab= [( ((0.19) * val_tab[0]) - 87.0),]
            for i in range(1, self.nbre_point.get()) :
                val_tab_ch0.append(val_tab[i*2])
                power_tab.append( ((0.19) * val_tab[i*2]) - 87.0)
            
            self.ser.close()
            self.Spectre = power_tab
            self.Graph_dessin()
    
    def LTDZ_version(self):    
        BAUDRATE = 57600
        try :
            self.ser = serial.Serial(self.port.get(), BAUDRATE)     
        except Exception:
            tk.messagebox.showerror("Erreur Serial", "le port serie n'existe pas")
            print ("error open serial port: " + str(self.port.get()))
            #self.destroy()
        else :
            self.ser.write(b'\x8f' + b'\x76')   #  b'\x8f' + b'v'
            time.sleep(0.5)
            self.version.set("Ver : " + str(ord(self.ser.read())/100))
            #self.ltdz_version.setText(version)
            self.ser.close()
    
if __name__ == "__main__":
	app = PyLTDZ(None)
	app.title('PyLTDZ - v1')
	app.mainloop()
