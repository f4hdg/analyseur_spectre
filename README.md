# Analyseur_Spectre

Création d'analyseur de spectre

##  ASRF-34-4400
Projet de création d'un analyseur de base d'Arduino + ADF4351 + Melangeur + AD8703

## PyLTDZ
Interface utilisateur en python d'une plateforme LTDZ pour la mesure du spectre, générateur de fréquence et mesure de puissance.
- Python 3
- Matplotlib

![GUI Spectre](./doc/pyLTDZ_spectre.jpg)
![GUI Puissance](./doc/pyLTDZ_power.jpg)